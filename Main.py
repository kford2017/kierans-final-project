__author__ = 'jhoeflich2017'
import Player

# Character Creation
character = Player.Player('butt', 3, 20, 0, 1, 0)
new_name = input('Enter a name for your character! ')
character.set_name(new_name)
new_health = int(input('Enter your health for testing purposes '))
character.set_health(new_health)
new_attack = int(input('Enter an attack for testing '))
character.set_attack(new_attack)
money = int(input('Enter a money value for testing '))
character.set_money(money)
new_speed = int(input('Enter a value for speed for testing '))
character.set_speed(new_speed)
potions = 0

print('My name is', character.get_name())
print('My health is', str(character.get_health()))
print('My money is', str(character.get_money()))
print('My attack is', str(character.get_attack()))
print('My speed is', str(character.get_speed()))


#Monster Imports
import sys
sys.path.append('../..')
from monsters import zombie
from monsters import skeleton
from monsters import rat
from monsters import landoctopus
from monsters import shekelton
from monsters import ceilingjiggly
from monsters import jackpizza

# Difficulty Selection
diff = input('Select a difficulty. Easy, Normal, Hard ')
if diff == 'Easy':
    #The order is attack, health, gold dropped and speed
    skeleton = skeleton.Skeleton('Skeleton', 3, 7, 50, 2)
    zombie = zombie.Zombie('Zombie', 2, 5, 30, 1)
    rat = rat.Rat('Rat', 1, 2, 10, 3)
    landoctopus = landoctopus.Landoctopus('Mutant Octopus', 5, 11, 80, 2)
    shekelton = shekelton.Shekelton('Lord Shekelton', 6, 15, 400, 3)
    ceilingjiggly = ceilingjiggly.Ceilingjiggly('Ceiling Jiggly', 3, 3, 10, 1)
    jackpizza = jackpizza.Jackpizza('Jack Pizza', 7, 30, 0, 5)
if diff == 'Normal':
    #The order is attack, health, gold dropped and speed
    skeleton = skeleton.Skeleton('Skeleton', 5, 9, 50, 2)
    zombie = zombie.Zombie('Zombie', 4, 7, 30, 1)
    rat = rat.Rat('Rat', 3, 4, 10, 3)
    landoctopus = landoctopus.Landoctopus('Mutant Octopus', 7, 13, 80, 2)
    shekelton = shekelton.Shekelton('Lord Shekelton', 8, 17, 400, 3)
    ceilingjiggly = ceilingjiggly.Ceilingjiggly('Ceiling Jiggly', 5, 5, 10, 1)
    jackpizza = jackpizza.Jackpizza('Jack Pizza', 9, 32, 0, 5)
if diff == 'Hard':
    #The order is attack, health, gold dropped and speed
    skeleton = skeleton.Skeleton('Skeleton', 6, 10, 50, 2)
    zombie = zombie.Zombie('Zombie', 5, 8, 30, 1)
    rat = rat.Rat('Rat', 4, 5, 10, 3)
    landoctopus = landoctopus.Landoctopus('Mutant Octopus', 8, 14, 80, 2)
    shekelton = shekelton.Shekelton('Lord Shekelton', 9, 18, 400, 3)
    ceilingjiggly = ceilingjiggly.Ceilingjiggly('Ceiling Jiggly', 6, 6, 10, 1)
    jackpizza = jackpizza.Jackpizza('Jack Pizza', 10, 33, 0, 5)

"""
Difficulty Functionality Test
print('Skeleton health is ', str(zombie.get_health()))
print('Skeleton attack is ', str(zombie.get_attack()))
print('Skeleton money is ', str(zombie.get_money()))
print('Skeleton speed is ', str(zombie.get_speed()))
"""

# Monster Death Functions
def skeletondeath(money):
    if skeleton.get_health() <= 0:
        money += 50
        character.set_money(money)
        print('You defeated the monster!')

def zombiedeath(money):
    if zombie.get_health <= 0:
        money += 30
        character.set_money(money)
        print('You defeated the monster!')

def ratdeath(money):
    if rat.get_health <= 0:
        money += 10
        character.set_money(money)
        print('You defeated the monster!')

def landoctopusdeath(money):
    if landoctopus.get_health <= 0:
        money += 80
        character.set_money(money)
        print('You defeated the monster!')

def shekeltondeath(money):
    if shekelton.get_health <= 0:
        money += 400
        character.set_money(money)
        print('You defeated the monster!')
def ceilingjigglydeath(money):
    if ceilingjiggly.get_health <= 0:
        money += 10
        character.set_money(money)
        print('You defeated the monster!')
def jackpizzadeath():
    if jackpizza.get_health <= 0:
        print('insert some lore crap here kieran')



# Movement System
f = 0
r = 0
while f < 10 and r < 10:
    direction = input('Which direction do you want to go?(Forward(w), Left(a), Back(s), Right(d)) ')

    if direction == 'w':
        f += 1
        print(f, 'units forward', r, 'units right')
    if direction == 's':
        f -= 1
        print(f, 'units forward', r, 'units right')
    if direction == 'a':
        r -= 1
        print(f, 'units forward', r, 'units right')
    if direction == 'd':
        r += 1
        print(f, 'units forward', r, 'units right')
# Death Sequence
    if new_health == 0:
        print('You died')
        break
# Store Interaction
    if f == 1 and r == 1:
        selection = input('Welcome to BUTT STORE!?! WHAT DO YOU WANT? a)Health Potion(10g) b)Sword(30g) c)Boots O Speed(50g) d)Armor(50) ')
        assert isinstance(selection, object)
        if selection == 'Health Potion' or 'a':
            potions += 1
            money -= 10
            character.set_money(money)
        if selection == 'Sword':
            money -= 30
            character.set_money(money)
            new_attack += 10
            character.set_attack(new_attack)
            print('Your attack is', str(character.get_attack()))
            print('Your money is', str(character.get_money()))
            print('You have', str(potions), 'potions')
        if selection == 'Boots O Speed':
            money -= 50
            character.set_money(money)
            character.set_speed += 3
            character.get_speed()
        if selection == 'Armor':
            money -= 50
            character.set_money(money)
            character.set_health += 5
            character.get_health()


